﻿using UnityEngine;
using System.Collections;

public class Player {

    private int supplies_, playerNumber_;

    public int Supplies //getter/setter
    {
        get { return supplies_; }

        set
        {
            supplies_ = value;
        }
    }

    public int PlayerNumber //getter/setter
    {
        get { return playerNumber_; }

        set
        {
            playerNumber_ = value;
        }
    }

    public Player(int startingSupplies, int playerNumber)
    {
        supplies_ = startingSupplies;
        playerNumber_ = playerNumber;
    }

}
