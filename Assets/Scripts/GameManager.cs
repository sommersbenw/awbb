﻿using Gamelogic.Grids;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {

    private IEnumerable<RectPoint> movePointsInRange;
    private IEnumerable<RectPoint> attackPointsInRange;
    private PointList<RectPoint> enemiesInRange = new PointList<RectPoint>();
    private Player currentPlayer;
    private RectPoint clickedPoint;
    private RectPoint originPoint, moveToPoint, attackedPoint, buildingPoint;

    private Startup startup;
    private InGameGUI inGameGUI;
    private ContextMenu contextMenu;
    private GameObject root;

    private TerrainGrid terrainGrid;
    private FeatureGrid featureGrid;
    private UnitGrid unitGrid;
    private IMap3D<RectPoint> map;
    private List<Player> playerList;

    private State state_;
    private enum State
    {
        WAITING,
        MOVING,
        ATTACKING,
        BUILDING
    }

    public static GameManager instance;

    void Awake ()
    {
        FillFromStartup();
    }

    // Use this for initialization
    void Start () {
        instance = GetComponent<GameManager>();
        root = GameObject.Find("GridAnchor");
        inGameGUI = GameObject.Find("Active").GetComponent<InGameGUI>();
        contextMenu = GameObject.Find("ContextMenu").GetComponent<ContextMenu>();
        state_ = State.WAITING;
        StartTurn();
    }

    private void FillFromStartup()
    { //TODO this could be refactored, doesnt seem to be a big performance issue though. 
        startup = new Startup();
        terrainGrid = startup.TerrainGrid;
        featureGrid = startup.FeatureGrid;
        unitGrid = startup.UnitGrid;
        map = startup.Map;
        playerList = startup.PlayerList;
        currentPlayer = playerList.First();
    }

    private void StartTurn()
    {
        int playerFundsIncrease = GetCurrentIncome();
        currentPlayer.Supplies += playerFundsIncrease;
        UpdateMySupplies();
    }

    private int GetCurrentIncome()
    {
        int propertyCount = 0;
        foreach (var point in featureGrid)
        {
            if (featureGrid[point] != null)
            {
                if (featureGrid[point].Owner == currentPlayer.PlayerNumber)
                {
                    propertyCount++;
                }
            }
        }
        return propertyCount;
    }

    void Update()
    {
        //currently nothing, controls are moved to Zoomer
    }

    public void ClearAllSelections()
    {
        foreach (var point in terrainGrid)
        {
            terrainGrid[point].ClearShading();
            if (featureGrid[point] != null)
            {
                featureGrid[point].ClearShading();
            }
        }
        originPoint = RectPoint.Zero;
        moveToPoint = RectPoint.Zero;
        attackedPoint = RectPoint.Zero;
        buildingPoint = RectPoint.Zero;
        inGameGUI.DisableMainMenu();
        inGameGUI.DisableBuildUnitMenu();
        inGameGUI.DisableContextMenu();
        inGameGUI.DisableBackButton();
        state_ = State.WAITING;
    }

    private void EndUnitTurn()
    {
        state_ = State.WAITING;
        unitGrid[moveToPoint].UpdateIsActive(false);
    }

    private RectPoint GetClickedPoint()
    {
        Vector3 cameraDepthCorrect = new Vector3(Input.mousePosition.x, Input.mousePosition.y, -Camera.main.gameObject.transform.position.z);
        Vector3 worldPosition = GridBuilderUtils.ScreenToWorld(root, cameraDepthCorrect);
        return map[worldPosition];
    }

    public bool IsPointOnTheMap(Vector3 point)
    {
        Vector3 cameraDepthCorrect = new Vector3(point.x, point.y, -Camera.main.gameObject.transform.position.z);
        RectPoint cameraPoint = map[cameraDepthCorrect];
        if (terrainGrid.Contains(cameraPoint)) { return true; }
        else { return false; }

    }

    public void ProcessPrimaryInput()
    {
        clickedPoint = GetClickedPoint();

        if (!terrainGrid.Contains(clickedPoint)) { return; }

        if (inGameGUI.IsPointOnAnyActiveMenus(map[clickedPoint])) { return; }
        else if (inGameGUI.AreThereAnyActiveMenus())
        {
            ClearAllSelections();
            return;
        }

        if (unitGrid[clickedPoint] != null)
        {
            switch (state_)
            {
                case State.WAITING:
                    inGameGUI.ShowBackButton();
                    if (unitGrid[clickedPoint].owner == currentPlayer.PlayerNumber && unitGrid[clickedPoint].IsActive)
                    {
                        StartUnitMovement();
                    }
                    else
                    {
                        inGameGUI.ShowMainMenu(map[clickedPoint]);
                    }
                    break;
                case State.MOVING:
                    if (terrainGrid[clickedPoint].HasMovementShading)
                    {
                        moveToPoint = clickedPoint;
                        AttemptUnitMovement();
                    }
                    break;
                case State.ATTACKING:
                    if (terrainGrid[clickedPoint].HasAttackShading)
                    {
                        attackedPoint = clickedPoint;
                        AttemptUnitMovement();
                        ExecuteAttack();
                    }
                    break;
            }
        }
        else if (featureGrid[clickedPoint] != null)
        {
            switch (state_)
            {
                case State.WAITING:
                    inGameGUI.ShowBackButton();
                    if (featureGrid[clickedPoint].CanBuild && featureGrid[clickedPoint].Owner == currentPlayer.PlayerNumber)
                    {
                        buildingPoint = clickedPoint;
                        state_ = State.BUILDING;
                        inGameGUI.ShowBuildUnitMenu(currentPlayer);
                    }
                    else
                    {
                        inGameGUI.ShowMainMenu(map[clickedPoint]);
                    }
                    break;
                case State.MOVING:
                    if (terrainGrid[clickedPoint].HasMovementShading)
                    {
                        moveToPoint = clickedPoint;
                        CheckIfCapturable();
                        inGameGUI.ShowContextMenu(map[moveToPoint]);
                    }
                    break;
                case State.ATTACKING:
                    //Do Nothing
                    break;
            }
        }
        else if (terrainGrid[clickedPoint] != null)
        {
            switch (state_)
            {
                case State.WAITING:
                    inGameGUI.ShowBackButton();
                    inGameGUI.ShowMainMenu(map[clickedPoint]);
                    break;
                case State.MOVING:
                    if (terrainGrid[clickedPoint].HasMovementShading)
                    {
                        moveToPoint = clickedPoint;
                        contextMenu.UpdateButtons(moveToPoint, false);
                        inGameGUI.ShowContextMenu(map[moveToPoint]);
                    }
                    break;
                case State.ATTACKING:
                    //Do Nothing
                    break;
            }
        }
    }

    private void CheckIfCapturable()
    {
        if (featureGrid[moveToPoint].Owner != currentPlayer.PlayerNumber && unitGrid[originPoint].canCapture)
        {
            contextMenu.UpdateButtons(moveToPoint, true);
        }
        else
        {
            contextMenu.UpdateButtons(moveToPoint, false);
        }
    }

    private void StartUnitMovement()
    {
        originPoint = clickedPoint;
        state_ = State.MOVING;
        movePointsInRange = GetMoveableCells(originPoint, unitGrid[originPoint].isFlyer);
        if (movePointsInRange != null)
        {
            ToggleMoveHighlight(true);
        }
    }
    private void ToggleMoveHighlight(bool highlightValue)
    {
        foreach (var point in movePointsInRange)
        {
            terrainGrid[point].UpdateMovementShading(highlightValue);
            if (featureGrid[point] != null)
            {
                featureGrid[point].UpdateMovementShading(highlightValue);
            }
        }
    }
    public void AttemptUnitMovement()
    {
        inGameGUI.DisableContextMenu();
        if (unitGrid[moveToPoint] == null || moveToPoint == originPoint) // TODO: Add unit merging functionality
        {
            if (moveToPoint != originPoint)
            {
                Vector3 unitDestinationPosition = map[moveToPoint];
                UnitCell tempUnit = unitGrid[originPoint];
                tempUnit.transform.localPosition = unitDestinationPosition;
                unitGrid[moveToPoint] = tempUnit;
                unitGrid[originPoint] = null;
            }
            if (state_ != State.ATTACKING)
            {
                EndUnitTurn();
                ClearAllSelections();
            }


        }

    }

    public void CaptureProperty()
    {
        featureGrid[moveToPoint].UpdateOwner(currentPlayer.PlayerNumber);
        AttemptUnitMovement();
    }

    public bool IsAnAttackPossible(RectPoint providedPoint )
    {
        if ((providedPoint == originPoint || unitGrid[originPoint].canAttackOnMove) && (CheckForenemiesInRange(providedPoint)))
        {
            return true;
        }
        return false;
    }

    public bool CheckForenemiesInRange(RectPoint attackingPoint)
    {
        bool hasEnemyNeighbor = false;
        attackPointsInRange = GetAttackableCells(attackingPoint, unitGrid[originPoint].attackMinRange, unitGrid[originPoint].attackMaxRange);
        enemiesInRange.Clear();
        foreach (RectPoint potentialEnemy in attackPointsInRange)
        {
            if (unitGrid[potentialEnemy] != null && unitGrid[potentialEnemy].owner != currentPlayer.PlayerNumber)
            {
                hasEnemyNeighbor = true;
                enemiesInRange.Add(potentialEnemy);
            }
        }
        return hasEnemyNeighbor;
    }

    public void HighlightEnemiesInRange()
    {
        inGameGUI.DisableContextMenu();
        state_ = State.ATTACKING;
        foreach (RectPoint neighbor in enemiesInRange)
        {
            terrainGrid[neighbor].UpdateAttackShading(true);
            if (featureGrid[neighbor] != null)
            {
                featureGrid[neighbor].UpdateAttackShading(true);
            }
        }
    }

    public void ExecuteAttack()
    {
        DamageUnit(moveToPoint, attackedPoint);
        if (unitGrid[attackedPoint].HitPoints <= 0)
        {
            KillUnit(attackedPoint);
        }
        else if (InCounterattackRange(moveToPoint, attackedPoint))
        {
            DamageUnit(attackedPoint, moveToPoint);
            if (unitGrid[moveToPoint].HitPoints <= 0)
            {
                KillUnit(moveToPoint);
            }
        }
        EndUnitTurn();
        ClearAllSelections();
    }

    private bool InCounterattackRange(RectPoint defenderPoint, RectPoint attackerPoint)
    {
        IEnumerable<RectPoint> counterRange;
        counterRange = GetAttackableCells(attackerPoint, unitGrid[attackerPoint].attackMinRange, unitGrid[attackerPoint].attackMaxRange);
        if (counterRange.Contains(defenderPoint) && unitGrid[attackerPoint].canAttackOnMove)
        {
            return true;
        }
        return false;
    }

    private void DamageUnit(RectPoint attacker, RectPoint defender)
    {
        float tempHP = unitGrid[defender].HitPoints;
        float attackPower = unitGrid[attacker].attackPower;
        attackPower = attackPower * (float)Math.Sqrt(unitGrid[attacker].HitPoints / 10);
        attackPower -= terrainGrid[defender].DefenseBonus;
        tempHP -= attackPower;
        unitGrid[defender].HitPoints = tempHP;
    }
    private void KillUnit(RectPoint killedUnitPoint)
    {
        Destroy(unitGrid[killedUnitPoint].gameObject);
        unitGrid[killedUnitPoint] = null;
    }

    public void EndTurn()
    {
        ClearAllSelections();
        int index = playerList.IndexOf(currentPlayer);
        currentPlayer = playerList[index == 0 ? 1 : index % (playerList.Count - 1)];
        inGameGUI.UpdateCurrentPlayerText("P" + currentPlayer.PlayerNumber + " Turn");
        foreach (RectPoint point in unitGrid)
        {
            if (unitGrid[point] != null)
            {
                unitGrid[point].UpdateIsActive(true);
            }
        }
        inGameGUI.DisableMainMenu();
        StartTurn();
    }

    public void BuildUnitFromFactory(UnitCell unit)
    {
        state_ = State.WAITING;
        inGameGUI.DisableBuildUnitMenu();
        unitGrid[buildingPoint] = unitGrid.CreateCell(buildingPoint, map, unit.name, currentPlayer.PlayerNumber);
        unitGrid[buildingPoint].UpdateIsActive(false);
        currentPlayer.Supplies -= unit.Cost;
        UpdateMySupplies();
    }

    private void UpdateMySupplies()
    {
        if (currentPlayer.PlayerNumber == 1) { inGameGUI.UpdatePlayer1Supplies(currentPlayer.Supplies); }
        else if (currentPlayer.PlayerNumber == 2) { inGameGUI.UpdatePlayer2Supplies(currentPlayer.Supplies); }
        //cry inside at the above code until we have our real floating GUI
    }

    public IEnumerable<RectPoint> GetMoveableCells(RectPoint point, bool canFly)
    {
        Func<RectPoint, RectPoint, int> ourGetCellMoveCost;
        if (canFly)
        {
            ourGetCellMoveCost = GetFlyingMovementCost;
        }
        else
        {
            ourGetCellMoveCost = GetGroundMovementCost;
        }


        IEnumerable<RectPoint> cellList = Algorithms.GetPointsInRange(
            terrainGrid,
            point,
            c => c.IsWalkable || canFly,
            ourGetCellMoveCost,
            unitGrid[point].movementRange);

        return cellList;
    }


    private int GetGroundMovementCost(RectPoint p1, RectPoint p2)
    {
        if (unitGrid[p2] != null && unitGrid[p2].owner != currentPlayer.PlayerNumber)
        {
            return 9999; //chop shop solutions!!!
        }
        return terrainGrid[p2].MovementCost; //don't factor in the cost of the starting tile, only the ending tile. 
    }

    private int GetFlyingMovementCost(RectPoint p1, RectPoint p2)
    {
        if (unitGrid[p2] != null && unitGrid[p2].owner != currentPlayer.PlayerNumber)
        {
            return 9999; //chop shop solutions!!!
        }
        return 1; //don't factor in the cost of the starting tile, only the ending tile. 
    }

    public IEnumerable<RectPoint> GetAttackableCells(RectPoint point, int minRange, int maxRange)
    {


        IEnumerable<RectPoint> minCellList = Algorithms.GetPointsInRange(
            terrainGrid,
            point,
            c => true,
            (p , q) => 1,
            minRange-1);

        IEnumerable<RectPoint> maxCellList = Algorithms.GetPointsInRange(
            terrainGrid,
            point,
            c => true,
            (p, q) => 1,
            maxRange);

        return maxCellList.Except(minCellList);
    }
}
