﻿using Gamelogic.Grids;
using System;
using UnityEngine;

public class UnitGrid : RectGrid<UnitCell>
{

    private GameObject container;
    private UnitCell baseCell, tank, infantry, artillery, fighter, robot;
    private UnitCell[] unitTypes;
    private int unitCounter = 0;


    public UnitGrid(int width, int height, GameObject terrainContainer): base(width, height)
    {
        Rectangle(width, height);
        container = terrainContainer;
        LoadUnitData();
    }

    private void LoadUnitData()
    {
        baseCell = Resources.Load<UnitCell>("Prefabs/UnitTile");

        //This will be replaced with reading from a file later.....

        infantry = UnitCell.Instantiate(baseCell);
        infantry.FillCellData(3, 2, 0, "128/Infantry", "Infantry", 2, true);
        tank = UnitCell.Instantiate(baseCell);
        tank.FillCellData(6, 6, 0, "128/Tank", "Tank", 7, false);
        artillery = UnitCell.Instantiate(baseCell);
        artillery.FillCellData(5, 6, 0, "128/Artillery", "Artillery", 6, false, false, 2, 3, false);
        fighter = UnitCell.Instantiate(baseCell);
        fighter.FillCellData(8, 3, 0, "128/Fighter", "Fighter", 9, false, true);
        robot = UnitCell.Instantiate(baseCell);
        robot.FillCellData(2, 8, 0, "128/Robot", "Robot", 12, true);

        unitTypes = new UnitCell[] { infantry, tank, artillery, fighter, robot };
        foreach (UnitCell cell in unitTypes)
        {
            cell.transform.parent = GameObject.Find("UnitList").transform;
        }
    }

    public UnitCell[] UnitTypes //getter
    {
        get { return unitTypes; }
    }

    public UnitCell CreateCell(RectPoint point, IMap3D<RectPoint> map, string type, int owner)
    {
        Vector3 creationPoint = map[point];
        UnitCell celltype = Array.Find<UnitCell>(unitTypes, t => t.name.Contains(type));
        UnitCell thiscell = UnitCell.Instantiate(celltype);
        SetPositionAndParent(thiscell, creationPoint, container);
        if (owner != 0) { thiscell.UpdateOwner(owner); }
        thiscell.name = "Unit" + unitCounter++;

        return thiscell;
    }

    private void SetPositionAndParent(SpriteCell thiscell, Vector3 creationPoint, GameObject container)
    {
        thiscell.transform.parent = container.transform;
        thiscell.transform.localScale = Vector3.one;
        thiscell.transform.localPosition = creationPoint;
    }
}
