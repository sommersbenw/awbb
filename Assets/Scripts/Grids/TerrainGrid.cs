﻿using Gamelogic.Grids;
using System;
using UnityEngine;

public class TerrainGrid : RectGrid<TerrainCell>
{

    private GameObject container;
    private TerrainCell baseCell, grass, desert, ocean;
    private TerrainCell[] terrainTypes;


    public TerrainGrid(int width, int height, GameObject terrainContainer): base(width, height)
    {
        Rectangle(width, height);
        container = terrainContainer;
        LoadTerrainData();
    }

    private void LoadTerrainData()
    {
        baseCell = Resources.Load<TerrainCell>("Prefabs/TerrainTile");

        //This will be replaced with reading from a file later.....

        grass = TerrainCell.Instantiate(baseCell);
        grass.FillCellData(true, 1, 0, "128/Grass", "grass");
        desert = TerrainCell.Instantiate(baseCell);
        desert.FillCellData(true, 2, 2, "128/Desert", "desert");
        ocean = TerrainCell.Instantiate(baseCell);
        ocean.FillCellData(false, 1, 0, "128/Ocean", "ocean");

        terrainTypes = new TerrainCell[] { grass, desert, ocean };
        foreach (TerrainCell cell in terrainTypes)
        {
            cell.transform.parent = GameObject.Find("BrandedPrefabs").transform;
        }
    }

    public TerrainCell CreateCell(RectPoint point, IMap3D<RectPoint> map, string type)
    {
        Vector3 creationPoint = map[point];
        TerrainCell celltype = Array.Find<TerrainCell>(terrainTypes, t => t.name.Contains(type));
        TerrainCell thiscell = TerrainCell.Instantiate(celltype);
        SetPositionAndParent(thiscell, creationPoint, container);
        thiscell.name = "T" + point.ToString();

        return thiscell;
    }

    private void SetPositionAndParent(SpriteCell thiscell, Vector3 creationPoint, GameObject container)
    {
        thiscell.transform.parent = container.transform;
        thiscell.transform.localScale = Vector3.one;
        thiscell.transform.localPosition = creationPoint;
    }
}

