﻿using Gamelogic.Grids;
using System;
using UnityEngine;

public class FeatureGrid : RectGrid<FeatureCell>
{

    private GameObject container;
    private FeatureCell baseCell, city, factory, HQ;
    private FeatureCell[] featureTypes;


    public FeatureGrid(int width, int height, GameObject featureContainer): base(width, height)
    {
        Rectangle(width, height);
        container = featureContainer;
        LoadFeatureData();

    }

    private void LoadFeatureData()
    {
        baseCell = Resources.Load<FeatureCell>("Prefabs/FeatureTile");

        //This will be replaced with reading from a file later.....

        city = FeatureCell.Instantiate(baseCell);
        city.FillCellData(true, 1, 2, 0, "128/City", "city", false);
        factory = FeatureCell.Instantiate(baseCell);
        factory.FillCellData(true, 1, 2, 0, "128/Factory", "factory", true);
        HQ = FeatureCell.Instantiate(baseCell);
        HQ.FillCellData(true, 1, 3, 0, "128/HQ", "hq", false);

        featureTypes = new FeatureCell[] { city, factory, HQ };
        foreach (FeatureCell cell in featureTypes) {
            cell.transform.parent = GameObject.Find("BrandedPrefabs").transform;
        }
    }

    public FeatureCell CreateCell(RectPoint point, IMap3D<RectPoint> map, string type, int owner)
    {
        Vector3 creationPoint = map[point];
        FeatureCell celltype = Array.Find<FeatureCell>(featureTypes, t => t.name.Contains(type));
        FeatureCell thiscell = FeatureCell.Instantiate(celltype);
        SetPositionAndParent(thiscell, creationPoint, container);
        thiscell.UpdateOwner(owner);
        thiscell.name = "F" + point.ToString();

        return thiscell;
    }

    private void SetPositionAndParent(SpriteCell thiscell, Vector3 creationPoint, GameObject container)
    {
        thiscell.transform.parent = container.transform;
        thiscell.transform.localScale = Vector3.one;
        thiscell.transform.localPosition = creationPoint;
    }
}
