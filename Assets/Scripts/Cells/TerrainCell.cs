﻿using UnityEngine;
using System.Collections;
using Gamelogic.Grids;

public class TerrainCell : SpriteCell, IGLScriptableObject
{
    public bool isWalkable_;
    public int movementCost_;
    public int defenseBonus_;
    private Sprite sprite_;
    private Color myColor;
    private bool attackShading_;
    private bool movementShading_;

    public void FillCellData(bool isWalkable, int movementCost, int defenseBonus, string spritePath, string type)
    {
        isWalkable_ = isWalkable;
        movementCost_ = movementCost;
        defenseBonus_ = defenseBonus;
        sprite_ = Resources.Load<Sprite>("Textures/" + spritePath);
        GetComponent<SpriteRenderer>().sprite = sprite_;
        this.name = type;
    }

    public bool HasAttackShading //getter
    {
        get { return attackShading_; }
    }
    public bool HasMovementShading //getter
    {
        get { return movementShading_; }
    }
    public bool IsWalkable //getter
    {
        get { return isWalkable_; }
    }
    public int MovementCost //getter
    {
        get { return movementCost_; }
    }
    public int DefenseBonus //getter
    {
        get { return defenseBonus_; }
    }
    public Sprite CellSprite //getter
    {
        get { return sprite_; }
    }

    public void ClearShading ()
    {
        movementShading_ = false;
        attackShading_ = false;
        myColor = Color.white;
        __UpdatePresentation(true);
    }

    public void UpdateAttackShading (bool attackShading)
    {
        attackShading_ = attackShading;
        myColor = attackShading_ ? Color.red : myColor;
        __UpdatePresentation(true);
    }
    public void UpdateMovementShading(bool movementshading)
    {
        movementShading_ = movementshading;
        myColor = movementShading_ ? Color.Lerp(myColor, Color.grey, 0.8f) : myColor;
        __UpdatePresentation(true);
    }

    public override void __UpdatePresentation(bool forceUpdate)
    {
        GetComponent<SpriteRenderer>().color = myColor;
    }
}
