﻿using UnityEngine;
using System.Collections;
using System;

public class Zoomer : MonoBehaviour {

    private float perspectiveZoomSpeed = 0.5f;        // The rate of change of the field of view in perspective mode.
    private float moveSpeed = 4.0f;

#if UNITY_IOS || UNITY_ANDROID || UNITY_WP8 || UNITY_IPHONE
    //private float holdTime = 0.8f; 
    //private float acumTime = 0;
    private Vector2 acumMove = Vector2.zero;
#endif

    private InputState inputState_;
    private enum InputState
    {
        WAITING,
        MOVING,
        CLEARED,
        ZOOMING
    }

    void Start()
    {
        inputState_ = InputState.WAITING;
    }

    void Update()
    {
#if UNITY_STANDALONE || UNITY_EDITOR
        if (Input.GetMouseButtonDown(0))
        {
            GameManager.instance.ProcessPrimaryInput();
        }
        else if (Input.GetMouseButtonDown(1))
        {
            GameManager.instance.ClearAllSelections();
        }
        else if (Input.mouseScrollDelta != Vector2.zero)
        {
            DesktopZoom();
        }

        if (Input.GetKey(KeyCode.RightArrow))
        {
            TryToMoveDirection(Vector3.right);
        }
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            TryToMoveDirection(Vector3.left);
        }
        if (Input.GetKey(KeyCode.UpArrow))
        {
            TryToMoveDirection(Vector3.up);
        }
        if (Input.GetKey(KeyCode.DownArrow))
        {
            TryToMoveDirection(Vector3.down);
        }
#elif UNITY_IOS || UNITY_ANDROID || UNITY_WP8 || UNITY_IPHONE
        if(Input.touchCount == 0)
        {
            //acumTime = 0; 
            inputState_ = InputState.WAITING;
        }
        else if(Input.touchCount == 1)
        {

            //acumTime += Input.GetTouch(0).deltaTime;
            
 
            //if(inputState_ == InputState.WAITING && acumTime >= holdTime)
            //{
            //    inputState_ = InputState.CLEARED;
            //    GameManager.instance.ClearAllSelections();
            //}
 
            if(Input.GetTouch(0).phase == TouchPhase.Ended && inputState_ == InputState.WAITING)
            {
                //acumTime = 0; 
                GameManager.instance.ProcessPrimaryInput();
            }
            else if (Input.GetTouch(0).phase == TouchPhase.Moved)
            {
                Touch touchZero = Input.GetTouch(0);
                acumMove += touchZero.deltaPosition;
                if (Vector3.Distance(acumMove , Vector2.zero) > 100 )
                {
                    inputState_ = InputState.MOVING;
                    MobileMove();
                }
            }       

        }
        else if (Input.touchCount == 2)
        {
            MobileZoom();
            inputState_ = InputState.ZOOMING;
        }
#endif
    }

    private void TryToMoveDirection(Vector3 direction)
    {
        Vector3 tempPosition = transform.position + direction * moveSpeed * Time.deltaTime;
        if (GameManager.instance.IsPointOnTheMap(tempPosition))
        {
            transform.position = tempPosition;
        }
        
    }

    private void MobileMove()
    {
        Touch touchZero = Input.GetTouch(0);
        Vector3 touchZeroInv = -touchZero.deltaPosition;
        TryToMoveDirection(touchZeroInv);
    }

    private void DesktopZoom()
    {
        GetComponent<Camera>().fieldOfView += -Input.GetAxis("Mouse ScrollWheel") * 20;
        // Clamp the field of view to make sure it's between 30 and 100.
        GetComponent<Camera>().fieldOfView = Mathf.Clamp(GetComponent<Camera>().fieldOfView, 29.9f, 100.1f);
    }

    private void MobileZoom()
    {
        // Store both touches.
        Touch touchZero = Input.GetTouch(0);
        Touch touchOne = Input.GetTouch(1);

        // Find the position in the previous frame of each touch.
        Vector2 touchZeroPrevPos = touchZero.position - touchZero.deltaPosition;
        Vector2 touchOnePrevPos = touchOne.position - touchOne.deltaPosition;

        // Find the magnitude of the vector (the distance) between the touches in each frame.
        float prevTouchDeltaMag = (touchZeroPrevPos - touchOnePrevPos).magnitude;
        float touchDeltaMag = (touchZero.position - touchOne.position).magnitude;

        // Find the difference in the distances between each frame.
        float deltaMagnitudeDiff = prevTouchDeltaMag - touchDeltaMag;

        // Otherwise change the field of view based on the change in distance between the touches.
        GetComponent<Camera>().fieldOfView += deltaMagnitudeDiff * perspectiveZoomSpeed;

        // Clamp the field of view to make sure it's between 30 and 100.
        GetComponent<Camera>().fieldOfView = Mathf.Clamp(GetComponent<Camera>().fieldOfView, 29.9f, 100.1f);
    }
}
