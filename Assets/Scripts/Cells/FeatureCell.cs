﻿using UnityEngine;
using System.Collections;
using Gamelogic.Grids;

public class FeatureCell : SpriteCell, IGLScriptableObject
{

    private bool isWalkable_;
    private int movementChange_;
    private int defenseChange_;
    private int owner_;
    private Sprite sprite_;
    private Color myColor;
    private Color ownerColor;
    public bool canBuild_;
    private bool attackShading_;
    private bool movementShading_;

    public void FillCellData(bool isWalkable, int movementChange, int defenseChange, int owner, string spritePath, string type, bool canBuild)
    {
        isWalkable_ = isWalkable;
        movementChange_ = movementChange;
        defenseChange_ = defenseChange;
        sprite_ = Resources.Load<Sprite>("Textures/" + spritePath);
        GetComponent<SpriteRenderer>().sprite = sprite_;
        this.name = type;
        owner_ = owner;
        canBuild_ = canBuild;
    }

    public bool HasAttackShading //getter
    {
        get { return attackShading_; }
    }
    public bool HasMovementShading //getter
    {
        get { return movementShading_; }
    }
    public bool IsWalkable //getter
    {
        get { return isWalkable_; }
    }
    public bool CanBuild //getter
    {
        get { return canBuild_; }
    }
    public int MovementChange //getter
    {
        get { return movementChange_; }
    }
    public int DefenseChange //getter
    {
        get { return defenseChange_; }
    }
    public Sprite CellSprite //getter
    {
        get { return sprite_; }
    }
    public int Owner //getter
    {
        get { return owner_; }
    }

    public void ClearShading()
    {
        attackShading_ = false;
        movementShading_ = false;
        myColor = ownerColor;
        __UpdatePresentation(true);
    }

    public void UpdateAttackShading(bool attackShading)
    {
        attackShading_ = attackShading;
        myColor = attackShading_ ? Color.Lerp(myColor, Color.red, 0.8f) : myColor;
        __UpdatePresentation(true);
    }
    public void UpdateMovementShading(bool movementshading)
    {
        movementShading_ = movementshading;
        myColor = movementShading_ ? Color.Lerp(myColor, Color.grey, 0.8f) : myColor;
        __UpdatePresentation(true);
    }

    public override void __UpdatePresentation(bool forceUpdate)
    {
        GetComponent<SpriteRenderer>().color = myColor;
    }

    public void UpdateOwner(int owner)
    {
        owner_ = owner;
        if (owner_ == 1)
        {
            ownerColor = new Color32(75, 151, 255, 255);
        }
        else if (owner_ == 2)
        {
            ownerColor = new Color32(191, 66, 66, 255);
        }
        else
        {
            ownerColor = Color.white;
        }
        myColor = ownerColor;
        __UpdatePresentation(true);
    }
}
