﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class UnitBuildMenu : MonoBehaviour {

    public Button buildUnitPrefab;
    private List<UnitCell> unitList;
    private List<Button> buttonList;

    // Use this for initialization
    void Start () {
        unitList = new List<UnitCell>();
        buttonList = new List<Button>();
        GameObject unitPrefabContainer = GameObject.Find("UnitList");
        for (int i = 0; i < unitPrefabContainer.transform.childCount; i++)
        {
            unitList.Add(unitPrefabContainer.transform.GetChild(i).gameObject.GetComponent<UnitCell>());
        }
        foreach (UnitCell unit in unitList)
        {
            Button button = Instantiate(buildUnitPrefab);
            button.name = "Build " + unit.name;
            SetPositionAndParent(button.transform, transform.position, transform.gameObject);

            string buttonText = "Build " + unit.name + " - "+ unit.Cost +" Supplies";
            button.transform.GetChild(0).GetComponent<Text>().text = buttonText;
            

            buttonList.Add(button);

        }
        GetComponent<RectTransform>().sizeDelta = new Vector2(250, 100 * buttonList.Count);
        foreach (Button button in buttonList)
        {
            int buttonIndex = buttonList.IndexOf(button);
            float yPosition = -(100 * buttonIndex - ((buttonList.Count * 100) - 100) / 2);
            button.transform.localPosition = new Vector3(0, yPosition);
        }
    }

    public void UpdateButtons(Player currentPlayer)
    {

        foreach (Button button in buttonList)
        {
            UnitCell unit = unitList[buttonList.IndexOf(button)];
            button.onClick.RemoveAllListeners();
            if (unit.Cost > currentPlayer.Supplies)
            {
                button.interactable = false;
            }
            else
            {
                button.interactable = true;
                button.onClick.AddListener(delegate { GameManager.instance.BuildUnitFromFactory(unit); });
            }
        }
    }

    private void SetPositionAndParent(Transform thisTransform, Vector3 creationPoint, GameObject container)
    {
        thisTransform.SetParent(container.transform);
        thisTransform.localScale = Vector3.one;
        thisTransform.localPosition = creationPoint;
    }
}
