﻿using Gamelogic.Grids;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ContextMenu : MonoBehaviour {

    public Button waitButton, attackButton, captureButton;
    private List<Button> buttonList;

    // Use this for initialization
    void Start() {
        waitButton = GameObject.Find("Wait").GetComponent<Button>();
        attackButton = GameObject.Find("Attack").GetComponent<Button>();
        captureButton = GameObject.Find("Capture").GetComponent<Button>();
        buttonList = new List<Button>() { waitButton, attackButton, captureButton };

    }

    public void UpdateButtons(RectPoint clickedPoint, bool capturableProperty)
    {
        if (capturableProperty)
        {
            ShowButton(captureButton);
        }
        else
        {
            HideButton(captureButton);
        }

        if (GameManager.instance.IsAnAttackPossible(clickedPoint))
        {
            ShowButton(attackButton);
        }
        else
        {
            HideButton(attackButton);
        }

        ResizeEverything();
    }

    private void ShowButton(Button button)
    {
        button.gameObject.SetActive(true);
        if (!buttonList.Contains(button))
        {
            buttonList.Add(button);
        }
    }

    private void HideButton(Button button)
    {
        button.gameObject.SetActive(false);
        if (buttonList.Contains(button))
        {
            buttonList.Remove(button);
        }
    }

    private void ResizeEverything()
    {
        GetComponent<RectTransform>().sizeDelta = new Vector2(200, 100 * buttonList.Count);
        foreach (Button button in buttonList)
        {
            int buttonIndex = buttonList.IndexOf(button);
            float yPosition = -(100 * buttonIndex - ((buttonList.Count * 100) - 100) / 2);
            button.transform.localPosition = new Vector3(0, yPosition);
        }
    }

}
