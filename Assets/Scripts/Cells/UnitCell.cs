﻿using UnityEngine;
using System.Collections;
using Gamelogic.Grids;
using System;

public class UnitCell : SpriteCell, IGLScriptableObject
{

    public int movementRange;
    public int attackPower;
    public int owner;
    public int attackMinRange;
    public int attackMaxRange;
    public bool canAttackOnMove;
    public bool canCapture;
    private float hitPoints_ = 10;
    private Sprite sprite_;
    private bool isActive_ = true;
    public bool isFlyer;
    private Color myColor;
    private Color ownerColor;
    private SpriteRenderer myHealth;
    private int cost_;


    public void FillCellData(int range, int power, int unitOwner, string spritePath, string type, int cost, bool canCapture, bool flyer = false, int attMin = 1, int attMax = 1, bool attackOnMove = true)
    {
        movementRange = range;
        attackPower = power;
        sprite_ = Resources.Load<Sprite>("Textures/" + spritePath);
        GetComponent<SpriteRenderer>().sprite = sprite_;
        this.name = type;
        owner = unitOwner;
        myColor = GetComponent<SpriteRenderer>().color;
        isFlyer = flyer;
        cost_ = cost;
        attackMinRange = attMin;
        attackMaxRange = attMax;
        canAttackOnMove = attackOnMove;
        return;
    }

    public int Cost //getter
    {
        get { return cost_; }
    }
    public float HitPoints //getter/setter
    {
        get { return hitPoints_; }

        set
        {
            hitPoints_ = value;
            if (hitPoints_ > 10) { hitPoints_ = 10; }
            if (hitPoints_ < 0) { hitPoints_ = 0; }
            UpdateMyHealth();
            __UpdatePresentation(true);
        }
    }

    private void UpdateMyHealth()
    {
        myHealth = transform.GetChild(0).GetComponent<SpriteRenderer>();
        int displayHitPoints = Convert.ToInt32(Math.Ceiling(hitPoints_));
        switch (displayHitPoints)
        {
            case 10:
            case 0:
                myHealth.enabled = false;
                break;
            default:
                myHealth.enabled = true;
                myHealth.sprite = Resources.Load<Sprite>("Textures/Health/" + displayHitPoints);
                break;
        }
    }

    public bool IsActive //getter
    {
        get { return isActive_; }
    }

    public void UpdateOwner(int owner)
    {
        this.owner = owner;
        if (this.owner == 1)
        {
            ownerColor = new Color32(75, 151, 255, 255);
        }
        else if (this.owner == 2)
        {
            ownerColor = new Color32(191, 66, 66, 255);
        }
        myColor = ownerColor;
        __UpdatePresentation(true);
    }

    public void UpdateIsActive(bool isActive)
    {
        isActive_ = isActive;
        myColor = isActive_ ? ownerColor : Color.Lerp(myColor, Color.grey, 0.8f);
        __UpdatePresentation(true);
    }

    public override void __UpdatePresentation(bool forceUpdate)
    {
        GetComponent<SpriteRenderer>().color = myColor;
    }
}
