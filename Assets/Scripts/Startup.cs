﻿using Gamelogic;
using Gamelogic.Grids;
using System;
using System.Collections.Generic;
using UnityEngine;

public class Startup {

    private int width, height, playerNumber;
    private float spriteHeight, spriteWidth;
    private string[,] terrainMap, playerMap;
    private string[,,] featureMap, unitMap;
    private GameObject terrainContainer, buildingContainer, unitContainer;
    private GameObject root;

    private TerrainGrid terrainGrid;
    private FeatureGrid featureGrid;
    private UnitGrid unitGrid;
    private IMap3D<RectPoint> map;
    private List<Player> playerList;

    public TerrainGrid TerrainGrid //getter
    {
        get { return terrainGrid; }
    }
    public FeatureGrid FeatureGrid //getter
    {
        get { return featureGrid; }
    }
    public UnitGrid UnitGrid //getter
    {
        get { return unitGrid; }
    }
    public IMap3D<RectPoint> Map //getter
    {
        get { return map; }
    }
    public List<Player> PlayerList //getter
    {
        get { return playerList; }
    }

    // Use this for initialization
    public Startup() {
        root = GameObject.Find("GridAnchor");
        CleanOutAllRootChildren();
        ReadInMapData();
        BuildPlayers();
        BuildGrids();
        CreateMap();
        FillGrids();
    }
    private void CleanOutAllRootChildren()
    {
        if (Application.isPlaying)
        {
            root.transform.DestroyChildren();
        }
        else
        {
            root.transform.DestroyChildrenImmediate();
        }
    }
    void ReadInMapData()
    {
        //This will be replaced with reading from a file later.....
        width = 10;
        height = 7;
        spriteWidth = 1.28f;
        spriteHeight = 1.28f;
        playerNumber = 2;

        playerMap = new string[2, 2] {
         {"1","0"},{"2","0"} };

        terrainMap = new string[7, 10] {
            { "ocean", "desert","grass","grass","grass","grass","grass","grass","desert","ocean" },
            { "desert", "grass","grass","grass","grass","ocean","grass","grass","grass","desert" },
            { "grass", "grass","desert","grass","grass","grass","desert","grass","grass","grass" },
            { "grass", "grass","grass","grass","grass","grass","grass","grass","grass","grass" },
            { "grass", "grass","grass","desert","grass","grass","grass","desert","grass","grass" },
            { "desert", "grass","grass","grass","ocean","grass","grass","grass","grass","desert" },
            { "ocean", "desert","grass","grass","grass","grass","grass","grass","desert","ocean" },};

        featureMap = new string[7, 10, 2] {
            { {null,null},{null,null},{"city","0"},{null,null},{null,null},{null,null},{null,null},{null,null},{"city","2"},{null,null} },
            { {null,null},{null,null},{null,null},{null,null},{"city","0"},{null,null},{null,null},{null,null}, {"factory","2"},{"city","2"} },
            { {"factory","1"},{null,null},{null,null},{null,null},{null,null},{"hq","2"},{null,null},{null,null},{null,null},{null,null} },
            { {null,null},{null,null},{null,null},{null,null},{null,null},{null,null},{null,null},{null,null},{null,null},{null,null} },
            { {null,null},{null,null},{null,null},{null,null},{"hq","1"},{null,null},{null,null},{null,null},{null,null},{"factory","2"} },
            { {"city","1"},{"factory","1"},{null,null},{null,null},{null,null},{"city","0"},{null,null},{null,null},{null,null},{null,null} },
            { {null,null},{"city","1"},{null,null},{null,null},{null,null},{null,null},{null,null},{"city","0"},{null,null},{null,null} },};

        unitMap = new string[7, 10, 2] {
            { {null,null},{null,null},{null,null},{null,null},{null,null},{null,null},{null,null},{null,null},{null,null},{null,null} },
            { {null,null},{null,null},{null,null},{null,null},{null,null},{null,null},{null,null},{null,null},{null,null},{null,null} },
            { {null,null},{null,null},{null,null},{null,null},{null,null},{null,null},{null,null},{"Infantry","2"},{"Artillery","2"},{"Fighter","2"} },
            { {"Robot","1"},{"Tank","1"},{"Infantry","1"},{null,null},{null,null},{null,null},{null,null},{"Infantry","2"},{"Tank","2"},{"Robot","2"} },
            { {"Fighter","1"},{"Artillery","1"},{"Infantry","1"},{null,null},{null,null},{null,null},{null,null},{null,null},{null,null},{null,null} },
            { {null,null},{null,null},{null,null},{null,null},{null,null},{null,null},{null,null},{null,null},{null,null},{null,null} },
            { {null,null},{null,null},{null,null},{null,null},{null,null},{null,null},{null,null},{null,null},{null,null},{null,null} },};

    }
    private void BuildPlayers()
    {
        playerList = new List<Player>();
        for (int i = 0; i < playerNumber; i++)
        {
            Player player = new Player(Convert.ToInt32(playerMap[i,1]),i+1);
            playerList.Add(player);
        }
    }

    void BuildGrids ()
    {
        ConstructGridContainers();
        terrainGrid = new TerrainGrid(width, height, terrainContainer);
        featureGrid = new FeatureGrid(width, height, buildingContainer);
        unitGrid = new UnitGrid(width, height, unitContainer);
    }
    private void ConstructGridContainers()
    {
        terrainContainer = new GameObject();
        SetUpContainer(terrainContainer, "TerrainContainer");
        buildingContainer = new GameObject();
        SetUpContainer(buildingContainer, "BuildingContainer");
        unitContainer = new GameObject();
        SetUpContainer(unitContainer, "UnitContainer");
    }
    private void SetUpContainer(GameObject container, string name)
    {
        container.name = name;
        container.transform.parent = root.transform;
    }
    private void CreateMap()
    {
        map = new RectMap(new Vector2(spriteWidth, spriteHeight))
            .ReflectAboutX()
            .WithWindow(Gamelogic.Grids.Examples.ExampleUtils.ScreenRect)
            .AlignMiddleCenter(terrainGrid)
            .To3DXY();
    }
    private void FillGrids()
    {
        foreach (RectPoint point in terrainGrid)
        {
            string terrainType = terrainMap[point.Y, point.X];
            terrainGrid[point] =  terrainGrid.CreateCell(point, map, terrainType);
        }
        foreach (RectPoint point in featureGrid)
        {
            if (featureMap[point.Y, point.X, 0] != null)
            {
                string featureType = featureMap[point.Y, point.X, 0];
                int featureOwner = Convert.ToInt32(featureMap[point.Y, point.X, 1],10);
                featureGrid[point] = featureGrid.CreateCell(point, map, featureType, featureOwner);
            }
        }
        foreach (RectPoint point in unitGrid)
        {
            if (unitMap[point.Y, point.X, 0] != null)
            {
                string unitType = unitMap[point.Y, point.X, 0];
                int unitOwner = Convert.ToInt32(unitMap[point.Y, point.X, 1], 10);
                unitGrid[point] = unitGrid.CreateCell(point, map, unitType, unitOwner);
            }
        }
    }

 
}
