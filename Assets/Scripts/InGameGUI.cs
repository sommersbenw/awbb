﻿using Gamelogic.Grids;
using System;
using UnityEngine;
using UnityEngine.UI;


public class InGameGUI : MonoBehaviour {

    private Text currentPlayerText, p1Supplies, p2Supplies;
    private UnitBuildMenu unitBuildMenu;
    private Image mainMenuPopup, contextMenuPopup;
    private Button backButton;
    private GameObject disabled, overviewGUI;
    private Vector3 visibilityAdjust = new Vector3(0, 0, -.5f);

    // Use this for initialization
    void Awake () {
        mainMenuPopup = GameObject.Find("MainMenu").GetComponent<Image>();
        contextMenuPopup = GameObject.Find("ContextMenu").GetComponent<Image>();
        unitBuildMenu = GameObject.Find("UnitBuildMenu").GetComponent<UnitBuildMenu>();
        currentPlayerText = GameObject.Find("CurrentPlayer").GetComponent<Text>();
        p1Supplies = GameObject.Find("Player1Supplies").GetComponent<Text>();
        p2Supplies = GameObject.Find("Player2Supplies").GetComponent<Text>();
        overviewGUI = GameObject.Find("GUI");
        backButton = GameObject.Find("Back").GetComponent<Button>();
        disabled = GameObject.Find("Disabled");
    }

    void Start()
    {

    }

    public void DisableMainMenu ()
    {
        mainMenuPopup.transform.SetParent(disabled.transform);
    }

    public void ShowMainMenu(Vector3 location)
    {
        mainMenuPopup.transform.position = location;
        MoveMenuDownAndRight(mainMenuPopup);
        mainMenuPopup.transform.SetParent(transform);
    }

    public void DisableContextMenu()
    {
        contextMenuPopup.transform.SetParent(disabled.transform);
    }

    public void ShowContextMenu(Vector3 location)
    {
        contextMenuPopup.transform.position = location;
        MoveMenuDownAndRight(contextMenuPopup);
        contextMenuPopup.transform.SetParent(transform);
    }

    public void DisableBuildUnitMenu()
    {
        unitBuildMenu.transform.SetParent(disabled.transform);
    }

    public void ShowBuildUnitMenu(Player currentPlayer)
    {
        unitBuildMenu.UpdateButtons(currentPlayer);
        unitBuildMenu.transform.SetParent(transform);
    }

    public void DisableBackButton()
    {
        backButton.transform.SetParent(disabled.transform);
    }

    public void ShowBackButton()
    {
        backButton.transform.SetParent(overviewGUI.transform);
    }

    public void UpdateCurrentPlayerText (string currentPlayer)
    {
        currentPlayerText.text = currentPlayer;
    }

    public void UpdatePlayer1Supplies(int supplies)
    {
        string stringSupplies = supplies.ToString();
        p1Supplies.text = "P1: " + stringSupplies + " Supplies";
    }

    public void UpdatePlayer2Supplies(int supplies)
    {
        string stringSupplies = supplies.ToString();
        p2Supplies.text = "P2: " + stringSupplies + " Supplies";
    }

    private void MoveMenuDownAndRight(Image menu)
    {
        float menuXMove = menu.rectTransform.rect.width / 200;
        float menuYMove = -menu.rectTransform.rect.height / 200;
        visibilityAdjust = new Vector3 (menuXMove, menuYMove,-.5f);
        menu.transform.Translate(visibilityAdjust);
    }
    public bool AreThereAnyActiveMenus()
    {
        bool activeMenu = false;
        if (mainMenuPopup.transform.parent == transform)
        {
            activeMenu = true;
        }
        else if (contextMenuPopup.transform.parent == transform)
        {
            activeMenu = true;
        }
        else if (unitBuildMenu.transform.parent == transform)
        {
            activeMenu = true;
        }
        return activeMenu;
    }

    public bool IsPointOnAnyActiveMenus(Vector3 location)
    {
        bool isOnMenu = false;
        if (mainMenuPopup.transform.parent == transform && mainMenuPopup.rectTransform.rect.Contains(location))
        {
            isOnMenu = true;
        }
        else if (contextMenuPopup.transform.parent == transform && contextMenuPopup.rectTransform.rect.Contains(location))
        {
            isOnMenu = true;
        }
        else if (unitBuildMenu.transform.parent == transform && unitBuildMenu.gameObject.GetComponent<Image>().rectTransform.rect.Contains(location))
        {
            isOnMenu = true;
        }
        return isOnMenu;
    }


}
